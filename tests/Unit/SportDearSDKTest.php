<?php

namespace Tests\Unit;

use App\SDKs\SportDearSDK;
use GuzzleHttp\Client;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SportDearSDKTest extends TestCase
{
   public function testARequestFetchesAllCompetitionsGamesAndCheckIfItCaches()
    {
        $this->assertNull(cache('football_competitions'));

        $competitions   = $this->sdk()->getAllCompetitions();

        $this->assertTrue(is_array($competitions));
        $this->assertNotNull(cache('football_competitions'));
    }

    public function testGetCurrentlyPlayingGames()
    {
        $this->assertTrue(is_array($this->sdk()->getInplay()));
    }

    public function sdk()
    {
        return new SportDearSDK;
    }
}
