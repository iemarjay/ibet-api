<?php
/**
 * Created by PhpStorm.
 * User: iemarjay
 * Date: 4/3/18
 * Time: 10:54 PM
 */

namespace App\Http\Controllers;


use App\SDKs\HttpClient;
use App\SDKs\SportDearSDK;

class InPlayController extends Controller
{
    public function index(HttpClient $client)
    {
        $response   = $client->loadFromCacheOrMakeRequest(
            'soccer.inplay',
            'https://api.sportdeer.com/v1/seasons/2/upcoming?populate=lineups',
            now()->addSecond()
        );

        return $response;
    }
}