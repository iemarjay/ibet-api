<?php
/**
 * Created by PhpStorm.
 * User: iemarjay
 * Date: 3/20/18
 * Time: 10:14 PM
 */

namespace App\SDKs;


use function foo\func;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;

class SportDearSDK
{
    public function getUpcomingGames()
    {
        $cacheKey   = 'football_upcoming_games';

        $response   = $this->client()
            ->loadFromCacheOrMakeRequest(
                $cacheKey,
                'https://api.sportdeer.com/v1/seasons/2/upcoming?populate=lineups',
                now()->addHour(1)
            );

        $this->parseResponseToUpcomingData($response);
    }

    public function parseResponseToUpcomingData($response)
    {
        dd($response);
//        {
//            "_id": 282,
//            "id_country": 46,
//            "id_league": 8,
//            "id_season": 2,
//            "id_stage": 1,
//            "fixture_status": "Postponed",
//            "fixture_status_short": "PSP",
//            "id_team_season_away": 14,
//            "id_team_season_home": 10,
//            "number_goal_team_away": 0,
//            "number_goal_team_home": 0,
//            "round": "31",
//            "schedule_date": "2018-03-16T20:00:00.000Z",
//            "stadium": "Wembley",
//            "team_season_away_name": "Newcastle United",
//            "team_season_home_name": "Tottenham",
//            "lineups": []
//        }
        collect($response->docs)->map(function ($game) {

        });
    }

    public function getInplay()
    {
        $cacheKey   = 'football_inplay_games';

        $response   = $this->client()
            ->loadFromCacheOrMakeRequest(
                $cacheKey,
                'https://api.sportdeer.com/v1/inplay?populate=lineups',
                now()->addSeconds(2)
            );

        return $this->parseResponseToInplayData($response);
    }

    protected function parseResponseToInplayData($response)
    {
        $competition    = collect($this->getAllCompetitions());

        return collect($response->docs)->map(function ($game) use($competition) {

            $getTeamLineUp  = function ($teamId) use($game) {
                return collect($game->lineups)
                    ->where('id_team_season', $teamId)
                    ->map(function ($player) {
                        return [
                            'id'    => $player->_id,
                            'name'  => $player->player_name,
                            'number'=> $player->shirtNumber
                        ];
                    })->toArray();
            };

            return [
                'id'    => $game->_id,
                'status'    => $game->fixture_status,
                'scores'    => "{$game->number_goal_team_home} : {$game->number_goal_team_away}",
                'away_team' => [
                    'id'    => $game->id_team_season_away,
                    'name'  => $game->team_season_away_name,
                    'goals' => $game->number_goal_team_away,
                    'lineup'   => $getTeamLineUp($game->id_team_season_away),
                ],
                'home_team' => [
                    'id'    => $game->id_team_season_home,
                    'name'  => $game->team_season_home_name,
                    'goals' => $game->number_goal_team_home,
                    'lineup'   => $getTeamLineUp($game->id_team_season_home),
                ],
                'competition' => $competition->where('id', $game->id_league)->toArray(),
                'time_elapsed'  => $game->elapsed,
            ];
        })->toArray();
    }

    public function getAllCompetitions()
    {
        if ($competitions   = cache('football_competitions')) {
            return json_decode($competitions);
        }

        $response   = $this->getRequest(
            'https://api.sportdeer.com/v1/leagues?populate=countries&populate=seasons'
        );

        $competitions   = $this->parseResponseToCompetitionData($response);

        $this->cacheCompetitionData($competitions);

        return $competitions;
    }

    protected function parseResponseToCompetitionData($response)
    {
        return collect($response->docs)->map(function ($competition) {
            $season = collect($competition->seasons)->last();

            return [
                'id'    => $competition->_id,
                'name'  => $competition->name,
                'logo'  => optional($competition)->logo_svg,
                'country'   => [
                    'id'    => $competition->countries->_id,
                    'name'    => $competition->countries->name,
                    'flag'    => optional($competition->countries)->square_flag,
                ],
                'season'    => [
                    'id'    => $season->_id,
                    'year'  => $season->years,
                ]
            ];
        })->toArray();
    }

    protected function cacheCompetitionData(array $competitions)
    {
        cache(['football_competitions' => json_encode($competitions)], now()->addDay());
    }

    protected function getAccessToken()
    {
        if ($accessToken    = cache('sports_dear_access_token')) {
            return $accessToken;
        }

        $response   = $this->requestAccessToken();

        $this->cacheAccessToken($response->access_token, $response->expires);

        return $response->access_token;
    }

    protected function requestAccessToken()
    {
        $response   = $this->getRequest(
            'https://api.sportdeer.com/v1/accessToken?refresh_token='. config('sports_dear.refresh_token')
        );

        return optional((object) [
            'access_token'  => $response->new_access_token,
            'expires'       => now()->addSeconds($response->expires_in_ms * 10**6)
        ]);
    }

    protected function cacheAccessToken($token, $expires)
    {
        return cache(['sports_dear_access_token' => $token], $expires);
    }

    protected function getRequest($uri, $options = [])
    {
        $response   = \Httpful\Request::get("$uri&access_token={$this->getAccessToken()}")->send();
//        $client     = new Client;
//        $response   = $client->get("$uri&access_token={$this->getAccessToken()}", $options);

        dd($response);
        if ($response->getStatusCode() == 200) {
            return json_decode($response->getBody()->getContents());
        }
    }

    protected function client()
    {
        return new HttpClient;
    }
}