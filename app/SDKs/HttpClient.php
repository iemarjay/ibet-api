<?php
/**
 * Created by PhpStorm.
 * User: iemarjay
 * Date: 3/21/18
 * Time: 11:33 AM
 */

namespace App\SDKs;


use GuzzleHttp\Client;

class HttpClient
{
    public function loadFromCacheOrMakeRequest($cacheKey, $uri, $cacheExpiry)
    {
        if ($data   = cache($cacheKey)) {
            return json_decode($data);
        }

        $response   = $this->getRequest($uri);

        cache([$cacheKey => json_encode($response)], $cacheExpiry);

        return $response;
    }


    protected function getRequest($uri, $options = [])
    {
        $client     = new Client;
        $response   = $client->get("$uri&access_token={$this->getAccessToken()}", $options);

        if ($response->getStatusCode() == 200) {
            return json_decode($response->getBody()->getContents());
        }
    }

    protected function getAccessToken()
    {
        if ($accessToken    = cache('sports_dear_access_token')) {
            return $accessToken;
        }

        $response   = $this->requestAccessToken();

        $this->cacheAccessToken($response->access_token, $response->expires);

        return $response->access_token;
    }

    protected function requestAccessToken()
    {
        $response   = $this->getRequest(
            'https://api.sportdeer.com/v1/accessToken?refresh_token='. config('sports_dear.refresh_token')
        );

        return optional((object) [
            'access_token'  => $response->new_access_token,
            'expires'       => now()->addSeconds($response->expires_in_ms * 10**6)
        ]);
    }

    protected function cacheAccessToken($token, $expires)
    {
        return cache(['sports_dear_access_token' => $token], $expires);
    }

}